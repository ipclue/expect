package com.ipclue.expect;

import java.util.regex.Matcher;

public class Match {

	private Matcher matcher;

	private String before;

	private String matched;

	private String after;

	public Match(Matcher matcher, String before, String matched, String after) {
		this.matcher = matcher;
		this.before = before;
		this.matched = matched;
		this.after = after;
	}

	public Matcher getMatcher() {
		return matcher;
	}

	public String getBefore() {
		return before;
	}

	public String getMatched() {
		return matched;
	}

	public String getAfter() {
		return after;
	}

}