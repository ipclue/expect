package com.ipclue.expect;

public final class ReturnAction extends Action {

	private String returnValue;

	public ReturnAction(String returnValue) {
		this.returnValue = returnValue;
	}

	public String getReturnValue() {
		return returnValue;
	}

	@Override
	public Action.Id getId() {
		return Id.RETURN;
	}

}
