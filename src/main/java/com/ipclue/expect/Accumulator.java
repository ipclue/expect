package com.ipclue.expect;

import java.io.IOException;
import java.io.InputStream;
import java.io.Writer;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Accumulator implements CharSequence {

	private Logger log = LoggerFactory.getLogger(this.getClass());

	// holds the accumulated data
	private final StringBuffer buffer = new StringBuffer();

	// the input stream to accumulate from
	private InputStream inputStream = null;

	// writers that will receive a copy of all data read from the input stream
	private List<Writer> logWriters = new ArrayList<Writer>(1);

	/**
	 * Create a new Accumulator not bound to an input stream.
	 */
	public Accumulator() {
	}

	/**
	 * Bind the accumulator to the provided input stream.
	 * 
	 * @param in
	 *            the input stream to read from
	 */
	public void setInputStream(final InputStream in) {
		this.inputStream = in;
	}

	/**
	 * Add a log writer. Log writers get a copy of all data that the accumulator
	 * reads.
	 * 
	 * @param writer
	 *            the Writer to write to
	 */
	public void addLogWriter(final Writer writer) {
		if (writer == null)
			throw new IllegalArgumentException("writer cannot be null");

		logWriters.add(writer);
	}

	/**
	 * Remove a log writer.
	 * 
	 * @param writer
	 *            the Writer to remove
	 */
	public void removeLogWriter(final Writer writer) {
		logWriters.remove(writer);
	}

	/**
	 * Remove all log writers.
	 */
	public void clearLogWriters() {
		logWriters.clear();
	}

	/**
	 * Reads available data from the input stream into the accumulator.
	 */
	public void fill() throws IOException {
		if (inputStream == null)
			throw new IOException("the accumulator is not yet bound to an input stream");

		int available = inputStream.available();
		if (available > 0) {
			// read data
			byte[] data = new byte[available];
			inputStream.read(data);

			// add data to buffer
			String str = new String(data);
			buffer.append(str);

			// write data to log writers
			for (Writer writer : logWriters) {
				try {
					writer.append(str);
				}
				catch (IOException e) {
					log.warn(
							"Caught IOException while writing to a log writer: "
									+ e.getMessage(), e);
				}
			}
		}
	}

	public char charAt(final int index) {
		return buffer.charAt(index);
	}

	public int length() {
		return buffer.length();
	}

	public CharSequence subSequence(final int start, final int end) {
		return buffer.subSequence(start, end);
	}

	public String substring(final int start, final int end) {
		return buffer.substring(start, end);
	}

	public String substring(final int start) {
		return buffer.substring(start);
	}

	public void delete(final int start, final int end) {
		buffer.delete(start, end);
	}

	public String toString() {
		return buffer.toString();
	}

}