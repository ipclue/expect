package com.ipclue.expect;

import java.io.IOException;
import java.io.InputStream;
import java.util.regex.Matcher;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Expect {

	private Logger log = LoggerFactory.getLogger(getClass());

	// the amount of time to wait for an expect match 
	private long defaultTimeoutMs = 20000L;

	private Accumulator accumulator = new Accumulator();

	private Timer timer = new Timer();

	/**
	 * Create a new Expect object that is not yet bound to an input stream.
	 * 
	 * @see setInputStream();
	 */
	public Expect() {
	}

	/**
	 * Create a new Expect object that reads from the provided input stream and
	 * writes to the provided output stream.
	 * 
	 * @param inputStream
	 *            the input stream to read from
	 */
	public Expect(InputStream inputStream) {
		this.setInputStream(inputStream);
	}

	/**
	 * Set the input stream to expect on.
	 * 
	 * @param inputStream
	 *            the input stream
	 */
	public void setInputStream(InputStream inputStream) {
		this.accumulator.setInputStream(inputStream);
	}

	/**
	 * Set the default match timeout. The match timeout cannot be configured to
	 * less than 1 millisecond. This is the timeout period if not specified when
	 * expect() is called. Defaults to 20 seconds.
	 * 
	 * @param defaultTimeoutMs
	 *            the default match timeout in milliseconds
	 */
	public void setDefaultTimeout(long defaultTimeoutMs) {
		if (defaultTimeoutMs < 0L)
			throw new IllegalArgumentException("timeout cannot be less than zero");
		this.defaultTimeoutMs = defaultTimeoutMs;
	}

	/**
	 * Get the default match timeout.
	 * 
	 * @return the default match timeout period in milliseconds
	 */
	public long getDefaultTimeout() {
		return this.defaultTimeoutMs;
	}

	/**
	 * Try to match the provided rules against data from the input stream before
	 * the default match timeout expires.
	 * 
	 * @param rules
	 *            rules to check in addition to any persistent rules
	 * @throws IOException
	 *             if there is a problem reading from the input stream
	 * @throws TimeoutException
	 *             if a rule doesn't match before the timeout expires
	 */
	public String any(Rule rule1, Rule... ruleN) throws IOException, TimeoutException {
		return any(defaultTimeoutMs, rule1, ruleN);
	}

	/**
	 * Try to match the provided rules against data from the input stream within
	 * a period of time.
	 * 
	 * @param timeoutMs
	 *            give up trying to match after this amount of time
	 * @param rules
	 *            rules to check in addition to any persistent rules
	 * @throws IOException
	 *             if there is a problem reading from the input stream
	 * @throws TimeoutException
	 *             if a rule doesn't match before the timeout expires
	 */
	public String any(long timeoutMs, Rule rule1, Rule... ruleN) throws IOException,
			TimeoutException {

		if (timeoutMs > 0) {
			log.debug("starting new timeout timer for " + timeoutMs + "ms");
			timer.start(timeoutMs);
		}

		Rule[] rules = concatenate(rule1, ruleN);

		try (Timer.Canceller closer = timer.getCanceller()) {
			log.debug("entering match loop");
			for (;;) {
				// try to get more data into the accumulator
				accumulator.fill();

				// process rules against accumulator contents
				for (Rule rule : rules) {
					/*
					 * check timeout flag. we do this in the inner loop because
					 * we don't know how many patterns there are or how long
					 * they take to match
					 */
					if (timer.getFired()) {
						log.debug("timeout timer expired, leaving match loop");
						throw new TimeoutException("timed out");
					}

					// similarly, check for interruption
					if (Thread.interrupted())
						throw new TimeoutException("interrupted");

					final Matcher matcher = rule.getPattern().matcher(accumulator);
					if (matcher.find()) {
						if (log.isDebugEnabled())
							log.debug("rule '" + rule + "' matched value '"
									+ matcher.group() + "'");

						String before = accumulator.substring(0, matcher.start());
						String matched = matcher.group();
						String after = accumulator.substring(matcher.end());

						Match match = new Match(matcher, before, matched, after);

						// fire the Rule callback
						log.debug("firing the callback of the matching rule");

						Action action = rule.onMatch(match);

						// remove data from accumulator
						accumulator.delete(0, matcher.end());

						// process the rule action
						switch (action.getId()) {
							case STOP:
								log.debug("rule action is STOP, cancelling the timout timer");
								timer.cancel();
								return match.getBefore();
							case RETURN:
								log.debug("rule action is RETURN, cancelling the timeout timer");
								timer.cancel();
								return ((ReturnAction) action).getReturnValue();
							case CONTINUE:
								if (((ContinueAction) action).getRestartTimer()) {
									log.debug("rule action is CONTINUE with timer restart, restarting the timeout timer");
									timer.restart();
								}
								break;
						} // action switch
					} // if rule matched
				} // for each rule
			} // main loop
		} // try
	}

	/*
	 * Concatenate all of the provided rules into a single array.
	 */
	private Rule[] concatenate(Rule rule1, Rule... ruleN) {
		Rule[] rules = new Rule[ruleN.length + 1];
		rules[0] = rule1;
		System.arraycopy(ruleN, 0, rules, 1, ruleN.length);
		return rules;
	}

}
