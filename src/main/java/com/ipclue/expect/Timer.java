package com.ipclue.expect;

import java.util.TimerTask;

/**
 * A timeout timer. This timer sets a fired flag when it expires. Additionally,
 * it implements the AutoCloseable interface so that it can be cancelled
 * automatically in a try-with-resources.
 * 
 * @author Mark Warren <mwarren@ipclue.com>
 */
public class Timer {

	private java.util.Timer timer = null;
	private volatile boolean fired = false;
	private long delayMs;

	/**
	 * Create a new Timer.
	 */
	public Timer() {
	}

	/**
	 * Start the timer.
	 * 
	 * @param delayMs
	 *            how long the timer should wait to fire, in milliseconds
	 */
	public void start(long delayMs) {
		// cancel the existing timer, just in case
		cancel();

		// reset state
		fired = false;
		this.delayMs = delayMs;

		// start the timer
		timer = new java.util.Timer("Expect Timer");
		timer.schedule(new TimerTask() {
			public void run() {
				fired = true;
			}
		}, delayMs);
	}

	/**
	 * Cancel the timer.
	 */
	public void cancel() {
		if (timer != null && !fired)
			timer.cancel();
	}

	/**
	 * Restart the timer. The timer is cancelled if already running, and then
	 * started again using the delay provided to the last invocation of start().
	 */
	public void restart() {
		cancel();
		start(delayMs);
	}

	/**
	 * Returns true if the timer fired, false otherwise.
	 * 
	 * @return the fired flag
	 */
	public boolean getFired() {
		return fired;
	}

	/**
	 * Returns an AutoCloseable that will cancel the timer on close.
	 * 
	 * @return an AutoCloseable
	 */
	public Canceller getCanceller() {
		return new Canceller() {
			public void close() {
				cancel();
			}
		};
	}

	public interface Canceller extends AutoCloseable {
		public void close();
	}

}