package com.ipclue.expect;

public abstract class Action {

	public static enum Id {
		STOP, RETURN, CONTINUE
	};

	public static final Action STOP = new StopAction();

	public static final Action CONTINUE_AND_RESTART_TIMER = new ContinueAction(true);

	public static final Action CONTINUE = new ContinueAction();

	public static Action RETURN(String returnValue) {
		return new ReturnAction(returnValue);
	}

	public abstract Action.Id getId();

}
