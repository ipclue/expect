package com.ipclue.expect;

public class TimeoutException extends Exception {

	private static final long serialVersionUID = 7917574840755387893L;

	public TimeoutException(String message) {
		super(message);
	}

}