package com.ipclue.expect;

public class ContinueAction extends Action {

	private boolean restartTimer = false;

	public ContinueAction() {
	}

	public ContinueAction(boolean restartTimer) {
		this.restartTimer = restartTimer;
	}

	public boolean getRestartTimer() {
		return restartTimer;
	}

	@Override
	public Action.Id getId() {
		return Id.CONTINUE;
	}

}
