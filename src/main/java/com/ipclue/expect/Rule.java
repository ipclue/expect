package com.ipclue.expect;

import java.util.regex.Pattern;

public abstract class Rule {

	private Pattern pattern;

	/**
	 * Create a new Rule that matches the provided Pattern.
	 * 
	 * @param pattern
	 *            the pattern to match
	 */
	public Rule(Pattern pattern) {
		this.pattern = pattern;
	}

	/**
	 * Create a new Rule that matches the provided regular expression string.
	 * 
	 * @param regex
	 *            the regular expression to match
	 */
	public Rule(String regex) {
		this.pattern = Pattern.compile(regex);
	}

	/**
	 * Create a new Rule that matches the provided regular expression string
	 * with the provided pattern option flags set.
	 * 
	 * @param regex
	 *            the regular expression string
	 * @param flags
	 *            the pattern option flags
	 */
	public Rule(String regex, int flags) {
		this.pattern = Pattern.compile(regex, flags);
	}

	/**
	 * Returns the Pattern object for this rule. This is either a pattern
	 * provided directly by the user, or one compiled from the user provided
	 * regular expression string and flags.
	 * 
	 * @return the Pattern
	 */
	public Pattern getPattern() {
		return pattern;
	}

	/**
	 * This method is called when the rule's pattern matches the accumulator. It
	 * is provided with a Match object containing the details of the match (
	 * including a Matcher object that can be used to extract capture groups,
	 * etc.). The method must return an action code that indicates what action
	 * Expect should take following the match.
	 * 
	 * @param match
	 *            the match details
	 * @return an action code
	 * @see Action
	 */
	public abstract Action onMatch(Match match);

}
