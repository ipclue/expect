package com.ipclue.expect.tests;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({ ExpectTest.class, RuleTest.class, AccumulatorTest.class })
public class AllTests {

}
