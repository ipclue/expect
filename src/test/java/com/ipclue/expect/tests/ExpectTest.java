package com.ipclue.expect.tests;

import static org.junit.Assert.assertEquals;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;

import org.junit.Test;

import com.ipclue.expect.Action;
import com.ipclue.expect.Expect;
import com.ipclue.expect.Match;
import com.ipclue.expect.Rule;
import com.ipclue.expect.TimeoutException;

public class ExpectTest {

	// TODO test no argument constructor
	// TODO test expect() before input stream is set
	// TODO test blocking expect()
	// TODO test continue with timeout reset

	@Test(timeout = 500)
	public void testMatchDetails() throws IOException, TimeoutException {
		Expect expect = new Expect(inputStream("foobarbaz"));
		String match = expect.any(new Rule("bar") {
			public Action onMatch(Match match) {
				assertEquals("match.getBefore()", "foo", match.getBefore());
				assertEquals("match.getMatched()", "bar", match.getMatched());
				assertEquals("match.getAfter()", "baz", match.getAfter());
				assertEquals("match.getMatcher() match", "bar", match.getMatcher()
						.group());
				return Action.STOP;
			}
		});
		assertEquals("expect() return value", "foo", match);
	}

	@Test(timeout = 500)
	public void testStopActionReturnValue() throws IOException, TimeoutException {
		Expect expect = new Expect(inputStream("foobar"));
		String match = expect.any(new Rule("foo") {
			public Action onMatch(Match match) {
				return Action.STOP;
			}
		});
		assertEquals("expect() return value", "", match);
	}

	@Test(timeout = 500)
	public void testReturnActionReturnValue() throws IOException, TimeoutException {
		Expect expect = new Expect(inputStream("foobar"));
		String match = expect.any(new Rule("foo") {
			public Action onMatch(Match match) {
				return Action.RETURN("fnord");
			}
		});
		assertEquals("expect() return value", "fnord", match);
	}

	@Test(timeout = 500)
	public void testContinue() throws IOException, TimeoutException {
		Expect expect = new Expect(inputStream("foobar"));

		// TODO what's taking so long in here?

		final StringBuffer match1 = new StringBuffer();
		String match2 = expect.any(new Rule("foo") {
			public Action onMatch(Match match) {
				match1.append(match.getMatched());
				return Action.CONTINUE;
			}
		}, new Rule("bar") {
			public Action onMatch(Match match) {
				return Action.RETURN(match.getMatched());
			}
		});

		assertEquals("continue rule", "foo", match1.toString());
		assertEquals("expect() return value", "bar", match2);
	}

	@Test(expected = IllegalArgumentException.class)
	public void testInvalidTimeout() {
		Expect expect = new Expect(inputStream(""));
		expect.setDefaultTimeout(-1);
	}

	@Test(expected = TimeoutException.class, timeout = 500)
	public void testTimeoutExpired1() throws IOException, TimeoutException {
		Expect expect = new Expect(inputStream("fnord"));
		expect.setDefaultTimeout(10);
		assertEquals(10L, expect.getDefaultTimeout());
		expect.any(new Rule("foo") {
			public Action onMatch(Match match) {
				return Action.STOP;
			}
		});
	}

	@Test(expected = TimeoutException.class, timeout = 500)
	public void testTimeoutExpired2() throws IOException, TimeoutException {
		Expect expect = new Expect(inputStream("fnord"));
		expect.any(10, new Rule("foo") {
			public Action onMatch(Match match) {
				return Action.STOP;
			}
		});
	}

	private InputStream inputStream(String data) {
		return new ByteArrayInputStream(data.getBytes());
	}

}
