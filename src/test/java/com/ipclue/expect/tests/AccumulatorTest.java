package com.ipclue.expect.tests;

import static org.junit.Assert.assertEquals;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStreamWriter;
import java.io.Writer;

import org.junit.Test;

import com.ipclue.expect.Accumulator;

public class AccumulatorTest {

	@Test(expected = IOException.class)
	public void testUnboundFillException() throws IOException {
		Accumulator a = new Accumulator();
		a.fill();
	}

	@Test
	public void testLogWriters() throws IOException {
		Accumulator a = new Accumulator();
		a.setInputStream(inputStream("foo"));

		ByteArrayOutputStream out1 = new ByteArrayOutputStream();
		Writer writer1 = new OutputStreamWriter(out1);

		ByteArrayOutputStream out2 = new ByteArrayOutputStream();
		Writer writer2 = new OutputStreamWriter(out2);

		a.addLogWriter(writer1);
		a.addLogWriter(writer2);

		a.fill();
		writer1.flush();
		writer2.flush();

		assertEquals("foo", out1.toString());
		assertEquals("foo", out2.toString());

		a.removeLogWriter(writer2);
		a.setInputStream(inputStream("bar"));

		a.fill();
		writer1.flush();
		writer2.flush();

		assertEquals("foobar", out1.toString());
		assertEquals("foo", out2.toString());

		a.clearLogWriters();
		a.setInputStream(inputStream("baz"));

		a.fill();
		writer1.flush();
		writer2.flush();

		assertEquals("foobar", out1.toString());
		assertEquals("foo", out2.toString());
	}

	@Test(expected = IllegalArgumentException.class)
	public void testAddNullLogWriter() {
		Accumulator a = new Accumulator();
		a.addLogWriter(null);
	}

	@Test
	public void testLogWriterException() throws IOException {
		Accumulator a = new Accumulator();
		a.addLogWriter(new Writer() {
			public void write(char[] cbuf, int off, int len) throws IOException {
				throw new IOException("test exception");
			}

			public void flush() throws IOException {
			}

			public void close() throws IOException {
			}
		});

		a.setInputStream(inputStream("foo"));
		a.fill();
		assertEquals("foo", a.toString());
	}

	private InputStream inputStream(String data) {
		return new ByteArrayInputStream(data.getBytes());
	}

}
