package com.ipclue.expect.tests;

import static org.junit.Assert.assertEquals;

import java.util.regex.Pattern;

import org.junit.Test;

import com.ipclue.expect.Action;
import com.ipclue.expect.Match;
import com.ipclue.expect.Rule;

public class RuleTest {

	@Test
	public void testPatternConstructor() {
		Pattern pattern = Pattern.compile("foo");
		Rule rule = new Rule(pattern) {
			public Action onMatch(Match match) {
				return null;
			}
		};
		assertEquals(pattern, rule.getPattern());
	}

	@Test
	public void testStringConstructor() {
		new Rule("foo") {
			public Action onMatch(Match match) {
				return null;
			}
		};
	}

	@Test
	public void testStringAndFlagsConstructor() {
		new Rule("foo", Pattern.CASE_INSENSITIVE) {
			public Action onMatch(Match match) {
				return null;
			}
		};
	}

}